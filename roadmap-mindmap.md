---
markmap:
  colorFreezeLevel: 2
---


# Documentation

## diagrams

- [Plant UML](#plant-uml.md)
- [PyLint](#pylint.md)
- [Component interaction]()
- Network topology maps
- Sequence diagrams

## data

- [data model](#data-model.md)
- [access pattern](#access-pattern.md)

## client

- **know-how level**
- **user manual**
- **deploy manual**
- **FAQ** 
- **use-case templates**
  + bird-eye view of the architecture
  + how provision works behind the scene

## monitoring

- **how to set alarms**
- **where are the monitors**
