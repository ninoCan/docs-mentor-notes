# Documentation Tools that Support Generation from CodeTool

| Description  | API | Type |
| ---  | --- | --- |
| DapperDox | Open source API documentation generator and server for OpenAPI Swagger specifications. Combines specifications with documentation, guides and diagrams, all of which can be authored in GitHub flavored Markdown. | REST |
| Doxygen | A widely-used documentation tool for C++, C, Objective-C, and several other programming languages. It generates documentation from source code comments and supports multiple output formats, including HTML, PDF, and LaTeX. |  Native code |
| Javadoc | A documentation tool specifically designed for generating API documentation for Java projects. It extracts comments from the source code and produces HTML-based documentation.    Native code |
| JSDoc |   A markup language used to annotate JavaScript source code files. Similar to Javadoc, but specialized to handle JavaScript's dynamic behavior. | Native code |
| RapiDoc | An interactive tool that lets you create visually appealing documents using the OpenAPI specification, with a console for making API calls and testing APIs.  | REST |
| ReadMe |  A platform that helps you create, manage, and maintain an online developer hub with different kinds of documents. Can automatically generate interactive API reference documentation from OpenAPI, RAML, or GraphQL specifications. Integrates with various development tools and services, such as GitHub, GitLab, and Slack. | REST |
| Native | Code |
| Read | the Docs   A documentation hosting platform that integrates with popular documentation generators like Sphinx and MkDocs. Automatically builds and deploys documentation from your source code repository, using the "docs as code" pattern.  | REST  Native Code |
| Redoc |   Produces web-ready documentation from an OpenAPI description or Swagger. | REST |
| Redocly | CLI Open source command-line tool for working with OpenAPI descriptions, developer portals, and other API lifecycle operations including API linting, enhancement, and bundling. |  REST |
| Sphinx |  A documentation generator commonly used in the Python ecosystem. Supports multiple markup languages, including reStructuredText (RST) and Markdown. Sphinx can generate various output formats, such as HTML, PDF, and ePub. | REST Native Code |
| Stoplight |   Platform that includes tools to generate API documentation from an OpenAPI specification. Includes templates for API Overview pages, Quick Start Guides, and How-to Tutorials. |  REST |
| Swagger | Codegen Generates an interactive HTML interface that allows users to explore the API endpoints, parameters, request/response examples, and more. | REST |
| ---  | --- | --- |
