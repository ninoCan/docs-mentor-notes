# Pages

Here, there are a list of online pages which might be useful to expand
knowledge and landscapes of documentations.


 - [OSS where to begin](https://contribute.cncf.io/contributors/getting-started/)
 - [Advent of Technical Documentation](https://jamesg.blog/category/advent-of-technical-writing/#main)
 - [Awesome Docs](https://github.com/testthedocs/awesome-docs)
 - [Faster management of tech docs](https://www.lucidchart.com/blog/how-to-create-and-manage-technical-documentation)
 - [5 steps to Tech Docs](https://plan.io/blog/technical-documentation/)
 - [6 steps to Tech Docs](https://blog.hubspot.com/service/technical-documentation)
 - [7 steps to Tech Docs](https://scribehow.com/library/how-to-write-technical-documentation)
 - [GitBook tips](https://www.gitbook.com/blog/how-to-write-technical-documentation-with-examples)
 - [Pluralsight Course: Technical ](https://app.pluralsight.com/library/courses/technical-writing-software-documentation/table-of-contents)
 - [Pluralsight Track: Business communication with Data](https://app.pluralsight.com/paths/skills/business-communications-with-data)
 - [Unusual Tech Docs types](https://document360.com/blog/technical-documentation/)
 - [C4 models Documentation](https://c4model.com/#CodeDiagram)
 - [Open Source Technical Documentation Essentials (LFC111)](https://trainingportal.linuxfoundation.org/courses/open-source-technical-documentation-essentials-lfc111)
 - [Creating Effective Documentation for Developers (LFC112)](https://trainingportal.linuxfoundation.org/courses/creating-effective-documentation-for-developers-lfc112)


 ## Language specific resources

 ### Python
 - [Real Python: doc tools](https://realpython.com/documenting-python-code/#documentation-tools-and-resources)
 - [Real Python: MkDoc](https://realpython.com/python-project-documentation-with-mkdocs/)